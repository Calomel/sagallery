#!/bin/bash

DIR=front/src/items
docker run --rm -it \
	     --volume "$PWD:/data" \
	     --workdir "/data/$DIR" \
	     --entrypoint $1 \
	     sagallery-back \
	     ${@:2}
