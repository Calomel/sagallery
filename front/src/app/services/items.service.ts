import { Injectable } from '@angular/core'; import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  constructor(private http: HttpClient) { }

  getItemsList() {
    return this.http.get("items/catalog.json");
  }
  getAlbum(id: string) {
    return this.http.get("items/" + id + "/list.json");
  }

}
