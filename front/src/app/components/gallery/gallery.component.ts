import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ItemsService } from '../../services/items.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
  providers: [ ItemsService ]
})
export class GalleryComponent implements OnInit {

  public catalog: object;
  public albums: Array<object>;
  public path: string;

  constructor(
		private router: Router,
		private route: ActivatedRoute,
    private service: ItemsService) {
    this.catalog = { 'albums': [] };
  }

  ngOnInit(): void {
    this.service.getItemsList()
      .subscribe((catalog: object) => {
        this.catalog = catalog;
        this.albums = new Array<object>(this.catalog['albums'].length);
        this.catalog['albums'].forEach((album, idx) => {
          this.albums[idx] = { 'items': [] };
          this.service.getAlbum(album).subscribe(
            a => {
              this.albums[idx] = a;
              this.albums[idx]['name'] = album;
              this.albums[idx]['path'] = 'items/' + album + '/';
            }
          )
        })
      });
    this.route.params.subscribe( params => {
      if ("id" in params) {
        this.path = params.id;
      } else {
        this.path = "";
      }

    })
  }

}
