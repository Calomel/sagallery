import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GalleryComponent } from './components/gallery/gallery.component'
import { HttpNotFoundComponent } from './components/http-not-found/http-not-found.component'

const routes: Routes = [
  { path: '', component: GalleryComponent },
  { path: 'items/:id', component: GalleryComponent },
  { path: '**', component: HttpNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
