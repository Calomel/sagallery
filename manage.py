from pathlib import Path
import os, argparse, subprocess, json, shutil

import yaml, ninja


def _is_newer(src, dst):
  if not dst.is_file():
    return True
  return os.path.getctime(str(dst)) < os.path.getctime(str(src))

def _relative_path(x, y):
    """
    Calculate the relative path of x w.r.t. y even when x is not nested in y
    """
    return Path(os.path.relpath(x, y))
def _symlink(src, dst):
    """
    Create symlink from {src} to {dst}. If {src} is already a symlink then
    override the link. Otherwise do nothing. This does the minimal damage by
    not accidentally removing data.

    Return true if a link is established or already exists.
    """
    print(src, '->', dst)
    if src.is_symlink():
        src.unlink()
    if not src.exists():
        src.symlink_to(dst)
        return True
    else:
        return False
def _copy(dst, src):
  shutil.copy(str(src), str(dst))

## Stages

def configure_build_system(args):
    assert len(args.paths) == 1, "Only a single path can be provided"
    pathIn = Path(args.paths[0])
    with open(pathIn / 'sagallery.yaml', 'r') as f:
        projectConfig = yaml.safe_load(f)

    # Calculate the fundamental paths used in this project
    pathBuild = Path.cwd()
    pathOut = pathBuild
    pathSelf = Path(__file__)
    pathLib = pathSelf.parent

    if args.verbose >= 1:
        print(f"Out path: {pathOut}")
        print(f"Build path: {pathBuild}")
        print(f"Library path: {pathLib}")

    if not args.abspath:
        pathSelf = _relative_path(pathSelf, pathBuild)
        pathLib = _relative_path(pathLib, pathBuild)

    writer = ninja.Writer(open('build.ninja', 'w'))

    writer.comment('Generated rules call manage.py')
    writer.rule('compile', f'python3 {pathSelf} compile $in $out')
    writer.rule('gather',  f'python3 {pathSelf} gather $in $out')
    writer.newline()

    for target in projectConfig['paths']:
        writer.comment(f'Target {target}')

        listPath = pathIn / target / 'list.yaml'

        writer.build(str(pathOut / target / 'list.json'), 'compile', \
                inputs=[str(pathIn / target / 'list.yaml')])

        # Phony rule to build an individual target
        writer.build(target, 'phony', \
                inputs=[ str(pathOut / target / 'list.json') ] )

    writer.build(str(pathOut / 'catalog.json'), 'gather', \
            inputs=[str(pathOut / target / 'list.json') for target in projectConfig['paths']])


def exec_compile(args):
  pathIn = Path(args.paths[0])
  pathOut = Path(args.paths[1])

  pathFolderIn = pathIn.parent
  pathFolderOut = pathOut.parent

  with open(pathIn, 'r') as f:
    folder = yaml.safe_load(f)
  # convert -define jpeg:size=300x300 2016_Matterhorn_by_the_dawn_of_the_night.jpg  -thumbnail 200x200 -unsharp 0x.5 /tmp/thumbnail.gif
  for item in folder['items']:
    imageIn = pathFolderIn / item['name']
    imageOut = pathFolderOut / item['name']
    imageThumb = (pathFolderOut / (item['name'] + '.thumb.gif'))
    #_symlink(imageOut, _relative_path(imageIn.absolute(), pathFolderOut))
    _copy(imageOut, imageIn)
    if _is_newer(imageIn, imageThumb):
      command = [
        'convert',
        '-define',
        'jpeg:size=600x600',
        str(imageIn),
        '-thumbnail',
        '600x600',
        '-unsharp',
        '0x.5',
        str(imageThumb),
      ]
      subprocess.run(command, check=True)

    item['name_thumb'] = imageThumb.name
    item['date'] = str(item['date'])

  # Produce consolidates json
  with open(pathFolderOut / 'list.json', 'w') as f:
    json.dump(folder, f)

  print(f"{pathIn} -> {pathOut}")
def exec_gather(args):
  pathCatalog = Path(args.paths[-1])
  pathOut = pathCatalog.parent
  pathFolders = [Path(x).relative_to(pathOut) for x in args.paths[:-1]]

  result = {
    'albums': [str(x.parent) for x in pathFolders],
  }
  with open(pathOut / 'catalog.json', 'w') as f:
    json.dump(result, f)
  print(f"{pathFolders} -> {pathOut}")

if __name__ == "__main__":
    executionDict = {
      "configure": configure_build_system,
      "compile": exec_compile,
      "gather": exec_gather,
    }
    parser = argparse.ArgumentParser()
    parser.add_argument('mode', type=str, \
            help='Mode of execution ' + '|'.join(executionDict.keys()))
    parser.add_argument('paths', nargs='+',\
            help='Paths of operation')
    parser.add_argument('--abspath', action='store_true', \
            help='Store absolute paths in configuration files (otherwise store relative)')
    parser.add_argument('-v', '--verbose', type=int, default=0, \
            help='Verbosity (default 0)')
    args = parser.parse_args()

    if args.mode in executionDict:
        executionDict[args.mode](args)
    else:
        print(f"Unknown mode {args.mode}")
