FROM ubuntu

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y \
	xindy \
	poppler-utils \
	imagemagick \
	python3-pip
RUN pip3 install \
	lxml \
	ninja \
	pyyaml
