#!/bin/bash

DIR=front/src/items
docker run --rm -it \
	     --volume "$PWD:/data" \
	     --workdir "/data/$DIR" \
	     --entrypoint python3 \
	     sagallery-back \
	     /data/manage.py configure ../../../$1
